package jimmy.sanmarkscoutgroup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddAttend extends Fragment implements DatePickerDialog.OnDateSetListener {

    static TextView attendDate;
    RadioGroup sections;
    EditText name;
    ListView boysListView;
    ArrayList<Boy> boysDataList;

    public AddAttend() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_attend, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        attendDate = view.findViewById(R.id.attend_date);
        attendDate.setOnClickListener(v -> openPicker(year, month, day));
        attendDate.setText(year + "/" + month + "/" + day);

        name = view.findViewById(R.id.name);
        boysListView = view.findViewById(R.id.boys_list);
        sections = view.findViewById(R.id.sections);
        sections.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = group.findViewById(checkedId);
            getData(radioButton.getText().toString());
        });
        name.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {searchNfilter(s.toString()); }
        });
    }

    private void openPicker(int year, int month, int day) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month-1, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        attendDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
    }

    public void getData(String section) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_BOYS,
                this::fetchResult,
                error -> Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("condition", "where section = '" + section + "'");
                Log.d("section ", section);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void fetchResult(String response) {
        try {
            boysDataList = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String id = object.getString("id");
                String name = object.getString("name");
                String phone = object.getString("phone");
                String birthdate = object.getString("birthdate");
                String grade = object.getString("grade");
                String address = object.getString("address");
                String section = object.getString("section");

                Boy boy = new Boy(id, name, phone, birthdate, grade, address, section);
                boysDataList.add(boy);
                names.add(name);
            }
            populateList(boysDataList);

        } catch (Exception ex) {
            populateList(new ArrayList<>());
            Toast.makeText(getActivity(), getResources().getString(R.string.error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void searchNfilter(String key) {
        ArrayList<Boy> boys = new ArrayList<>();
        for (Boy boy : boysDataList) {
            if (boy.getName().contains(key))
                boys.add(boy);
        }
        populateList(boys);
    }

    public void populateList(ArrayList<Boy> boys) {
        AttendAdapter adapter = new AttendAdapter(boys, getActivity());
        boysListView.setAdapter(adapter);
        boysListView.setOnItemClickListener((parent, view, position, id) -> {
            EditBoy.boy = boys.get(position);
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content, new EditBoy()).addToBackStack(null).commit();
        });
    }
}
