package jimmy.sanmarkscoutgroup;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class NewsAdapter extends BaseAdapter {

    private ArrayList<News> newsList;
    private Context context;

    NewsAdapter(ArrayList<News> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return newsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.news_item_layout,null);
        TextView name = convertView.findViewById(R.id.name);
        TextView description = convertView.findViewById(R.id.description);
        TextView seeMore = convertView.findViewById(R.id.see_more);
        name.setText(newsList.get(position).getName());
        description.setText(newsList.get(position).getDescription());
        seeMore.setOnClickListener(v ->{
            DisplayDocument.fileUrl = newsList.get(position).getFileURL();
            ((MainActivity)context).getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, new DisplayDocument())
                    .addToBackStack(null)
                    .commit();
        });
        return convertView;
    }
}
