package jimmy.sanmarkscoutgroup;

class Constants {
    private static final String URL = "http://njb-solutions.com/sanmark/api/";
    static final String ADD_BOY = URL.concat("addBoy.php");
    static final String GET_BOYS = URL.concat("getBoys.php");
    static final String EDIT_BOY = URL.concat("editBoy.php");
    static final String ADD_ATTEND = URL.concat("addAttend.php");
    static final String GET_ATTEND = URL.concat("getAttendees.php");
    static final String GET_DATES = URL.concat("getDates.php");
    static final String ADD_PERM = URL.concat("addPerm.php");
    static final String GET_PERMS = URL.concat("getPerms.php");
    static final String GET_DAY_PERMS = URL.concat("getDayPerms.php");
    static final String ADD_FEES = URL.concat("addFees.php");
    static final String GET_NEWS = URL.concat("getEvents.php");

}
