package jimmy.sanmarkscoutgroup;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.koushikdutta.ion.Ion;

import java.io.File;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class DisplayDocument extends Fragment {

    PDFView pdfView;
    WebView webView;
    ProgressBar progressBar;

    static String fileUrl;
    final String MS_VIEWER = "https://view.officeapps.live.com/op/embed.aspx?src=";

    public DisplayDocument() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_display_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        webView = view.findViewById(R.id.web_view);
        pdfView = view.findViewById(R.id.pdf_view);
        progressBar = view.findViewById(R.id.progress_bar);


        if (fileUrl.toLowerCase().endsWith(".pdf"))
            initPDF();
        else
            initDoc(fileUrl.contains("sanmark"));
    }

    private void initDoc(boolean isDocument) {
        ProgressDialog.getInstance().show(getActivity());
        pdfView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                ProgressDialog.getInstance().dismiss();
                if (url.contains("formResponse"))
                    pushNotification();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        if (isDocument)
            webView.loadUrl(MS_VIEWER + fileUrl);
        else
            webView.loadUrl(fileUrl);
    }

    private void pushNotification() {

    }

    private void initPDF(){
        webView.setVisibility(View.GONE);
        if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        else
            downloadAndView();
    }

    private void downloadAndView(){
        Ion.with(getActivity())
                .load(fileUrl)
                .progressBar(progressBar)
                .write(new File(Environment.getExternalStorageDirectory().getPath().concat("/temp.pdf")))
                .setCallback((e, result) -> {
                    progressBar.setVisibility(View.GONE);
                    if (result != null)
                        pdfView.fromFile(result).load();
                    else
                        Toast.makeText(getActivity(), "حدث خطأ، برجاء اعادة المحاولة", Toast.LENGTH_SHORT).show();
                });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                downloadAndView();
            else
                Toast.makeText(getActivity(), "اذن الوصول للذاكرة مطلوب", Toast.LENGTH_SHORT).show();
        }
    }


}
