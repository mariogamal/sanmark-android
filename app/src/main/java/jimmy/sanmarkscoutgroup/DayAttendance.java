package jimmy.sanmarkscoutgroup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DayAttendance extends Fragment implements DatePickerDialog.OnDateSetListener {

    TextView attDate;
    ListView boysList;
    RadioGroup sections;

    public DayAttendance() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_day_attendance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        attDate = view.findViewById(R.id.attend_date);
        attDate.setOnClickListener(v -> openPicker());
        attDate.setText(year+"/"+month+"/"+day);
        boysList = view.findViewById(R.id.boys_list);
        sections = view.findViewById(R.id.sections);
        sections.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = group.findViewById(checkedId);
            if (radioButton == null)
                return;
            getData(radioButton.getText().toString());
        });
    }

    public void openPicker() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        attDate.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
        sections.clearCheck();
    }

    public void getData(String section) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_ATTEND,
                response -> {
                    Log.d("Response", response);
                    fetchResult(response);
                },
                error -> Toast.makeText(getActivity(), "تعذر الاتصال", Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("att_date", attDate.getText().toString());
                params.put("section", section);
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void fetchResult(String result){
        try
        {
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("boy_name");
                names.add(name);
            }
            populateList(names);

        }catch (Exception ex)
        {
            Toast.makeText(getActivity(), "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<String>());
        }
    }

    public void populateList(ArrayList<String> names)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, names);
        boysList.setAdapter(adapter);
    }
}
