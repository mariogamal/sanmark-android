package jimmy.sanmarkscoutgroup;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ClericalFragment extends Fragment {

    public ClericalFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_clerical, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.add_boy).setOnClickListener(v -> openFragment(new AddBoy()));
        view.findViewById(R.id.get_boys).setOnClickListener(v -> openFragment(new GetBoys()));
        view.findViewById(R.id.add_attend).setOnClickListener(v -> openFragment(new AddAttend()));
        view.findViewById(R.id.get_attend).setOnClickListener(v -> showAttendDialog());
        view.findViewById(R.id.add_perm).setOnClickListener(v -> openFragment(new AddPermission()));
        view.findViewById(R.id.get_permissions).setOnClickListener(v -> showPermissionDialog());
        view.findViewById(R.id.annual_fees).setOnClickListener(v -> openFragment(new BoyFees()));
    }

    private void showAttendDialog(){
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.attend_type_dialog, null);
        dialog.setView(dialogView);
        dialogView.findViewById(R.id.boy_attend).setOnClickListener(v -> {
            dialog.dismiss();
            openFragment(new BoyAttendance());
        });
        dialogView.findViewById(R.id.daily_attend).setOnClickListener(v -> {
            dialog.dismiss();
            openFragment(new DayAttendance());
        });
        dialog.show();
    }

    private void showPermissionDialog(){
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.permission_type_dialog, null);
        dialog.setView(dialogView);
        dialogView.findViewById(R.id.boy_perm).setOnClickListener(v -> {
            dialog.dismiss();
            openFragment(new BoyPermissions());
        });
        dialogView.findViewById(R.id.daily_perm).setOnClickListener(v -> {
            dialog.dismiss();
            openFragment(new DayPermissions());
        });
        dialog.show();
    }

    private void openFragment(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content, fragment).addToBackStack(null).commit();
    }
}
