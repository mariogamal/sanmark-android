package jimmy.sanmarkscoutgroup;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AttendAdapter extends BaseAdapter {

    private ArrayList<Boy> boys;
    private Context context;

    AttendAdapter(ArrayList<Boy> boys, Context context) {
        this.boys = boys;
        this.context = context;
    }

    @Override
    public int getCount() {
        return boys.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.attend_list_item,null);
        TextView name = convertView.findViewById(R.id.name);
        final CheckBox attend = convertView.findViewById(R.id.attend);
        name.setText(boys.get(position).getName());
        attend.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                addAttend(boys.get(position).getName(),boys.get(position).getId(),boys.get(position).getSection(),attend);
        });
        return convertView;
    }


    private void addAttend(final String name, final String id, final String section, final CheckBox checkBox) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.ADD_ATTEND,
                response -> {
                    Log.d("Response", response);
                    if (response.contains("true")) {
                        checkBox.setEnabled(false);
                    }
                    else if (response.contains("Duplicate")) {
                        Toast.makeText(context, "الفرد مسجل", Toast.LENGTH_SHORT).show();
                        checkBox.setChecked(false);
                    }

                },
                error -> Toast.makeText(context, "تعذر الاتصال", Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("boy_id", id);
                params.put("boy_name",name);
                params.put("section",section);
                params.put("attend_date", AddAttend.attendDate.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

}
