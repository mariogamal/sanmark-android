package jimmy.sanmarkscoutgroup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class EditBoy extends Fragment implements DatePickerDialog.OnDateSetListener {

    static Boy boy;
    TextView birthdate;
    Spinner sectionSpinner;
    EditText name, phone, address, grade;

    public EditBoy() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_boy, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        name = view.findViewById(R.id.name);
        phone = view.findViewById(R.id.phone);
        address = view.findViewById(R.id.address);
        grade = view.findViewById(R.id.grade);
        sectionSpinner = view.findViewById(R.id.section_spinner);
        birthdate = view.findViewById(R.id.birthdate);
        birthdate.setOnClickListener(v -> openPicker());
        Button update = view.findViewById(R.id.save);
        update.setText(getResources().getString(R.string.update));
        update.setOnClickListener(v -> updateBoy());
        setBoyData();
    }

    private void setBoyData() {
        name.setText(boy.getName());
        phone.setText(boy.getPhone());
        address.setText(boy.getAddress());
        grade.setText(boy.getGrade());
        birthdate.setText(boy.getBirthdate());
        sectionSpinner.setSelection(getSectionIndex(boy.getSection()));
    }

    private int getSectionIndex(String section) {
        String[] sectionsArray = getResources().getStringArray(R.array.sections);
        for (int i = 0; i < sectionsArray.length; i++) {
            if (sectionsArray[i].equals(section))
                return i;
        }
        return 0;
    }

    private void updateBoy() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.EDIT_BOY,
                response -> {
                    if (response.contains("true"))
                        getActivity().onBackPressed();
                    else
                        Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    Log.d("Response", response);
                },
                error -> {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", boy.getId());
                params.put("name", name.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("birthdate", birthdate.getText().toString());
                params.put("address", address.getText().toString());
                params.put("grade", grade.getText().toString());
                params.put("section", sectionSpinner.getSelectedItem().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void openPicker() {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, 2000, 0, 1);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        birthdate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
    }
}
