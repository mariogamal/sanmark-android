package jimmy.sanmarkscoutgroup;

public class Boy {
    private String id;
    private String name;
    private String phone;
    private String birthdate;
    private String grade;
    private String address;
    private String section;
    private String fees;

    Boy(String id, String name, String phone, String birthdate, String grade, String address, String section) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.birthdate = birthdate;
        this.grade = grade;
        this.address = address;
        this.section = section;
    }

    Boy(String id, String name, String phone, String birthdate, String grade, String address, String section, String fees) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.birthdate = birthdate;
        this.grade = grade;
        this.address = address;
        this.section = section;
        this.fees = fees;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
