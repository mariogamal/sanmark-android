package jimmy.sanmarkscoutgroup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class BoyPermissions extends Fragment implements DatePickerDialog.OnDateSetListener{

    EditText name;
    ListView boysListView;
    TextView fromDate, endDate;
    ArrayList<Boy> boysDataList;
    boolean isFrom;

    public BoyPermissions() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_boy_permissions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        name = view.findViewById(R.id.name);
        boysListView = view.findViewById(R.id.boys_list);

        fromDate = view.findViewById(R.id.from_date);
        endDate = view.findViewById(R.id.end_date);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        endDate.setText(year + "/" + month + "/" + day);

        DisplayPermissions.startDate = fromDate.getText().toString();
        DisplayPermissions.endDate = endDate.getText().toString();

        fromDate.setOnClickListener(v -> openPicker(year, month, day, true));
        endDate.setOnClickListener(v -> openPicker(year, month, day, false));

        name.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                searchNfilter(s.toString());
            }
        });
        getData();
    }

    private void openPicker(int year, int month, int day, boolean isFrom) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month-1, day);
        this.isFrom = isFrom;
        dialog.show();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_BOYS,
                response -> fetchResult(response),
                error -> Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("condition", "");
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void fetchResult(String response) {
        try {
            boysDataList = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String id = object.getString("id");
                String name = object.getString("name");
                String phone = object.getString("phone");
                String birthdate = object.getString("birthdate");
                String grade = object.getString("grade");
                String address = object.getString("address");
                String section = object.getString("section");

                Boy boy = new Boy(id, name, phone, birthdate, grade, address, section);
                boysDataList.add(boy);
                names.add(name);
            }
            populateList(names, boysDataList);

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void searchNfilter(String key) {
        ArrayList<Boy> boys = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        for (Boy boy : boysDataList) {
            if (boy.getName().contains(key)) {
                boys.add(boy);
                names.add(boy.getName());
            }
        }
        populateList(names, boys);
    }

    public void populateList(ArrayList<String> names, final ArrayList<Boy> boys) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, names);

        boysListView.setAdapter(adapter);
        boysListView.setOnItemClickListener((parent, view, position, id) -> {
            DisplayPermissions.id = boys.get(position).getId();
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content, new DisplayPermissions()).addToBackStack(null).commit();
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (isFrom) {
            fromDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
            DisplayPermissions.startDate = fromDate.getText().toString();
        }
        else {
            endDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
            DisplayPermissions.endDate = endDate.getText().toString();
        }
    }
}
