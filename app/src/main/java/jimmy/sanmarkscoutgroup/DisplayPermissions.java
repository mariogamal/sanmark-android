package jimmy.sanmarkscoutgroup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DisplayPermissions extends Fragment {

    public static String id;
    ListView list;
    public static String startDate, endDate;

    public DisplayPermissions() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_display_permissions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = view.findViewById(R.id.permissionList);
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_PERMS,
                response -> {
                    Log.d("Response", response);
                    fetchResult(response);
                },
                error -> Toast.makeText(getActivity(), "تعذر الاتصال", Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id",id);
                params.put("condition", "and perm_date between '"+startDate+"' and '"+endDate+"'");
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void fetchResult(String result){
        try
        {
            ArrayList<String> perms = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String perm_date = object.getString("perm_date");
                String perm = object.getString("perm");
                perms.add(perm_date+"\n"+perm);
            }
            populateList(perms);

        }catch (Exception ex)
        {
            Toast.makeText(getActivity(), "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<>());
        }
    }

    public void populateList(ArrayList<String> perms) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, perms);
        list.setAdapter(adapter);
    }
}
