package jimmy.sanmarkscoutgroup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DisplayNews extends Fragment {

    ListView newsList;
    ArrayList<News> newsArrayList;
    public static String type = "";

    public DisplayNews() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_display_news, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getData();
        newsList = view.findViewById(R.id.news_list);
    }

    public void getData() {
        ProgressDialog.getInstance().show(getActivity());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_NEWS,
                response -> {
                    fetchResult(response);
                    ProgressDialog.getInstance().dismiss();
                },
                error -> {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    ProgressDialog.getInstance().dismiss();
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("type", type);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void fetchResult(String response) {
        Log.d("response", response);
        try {
            newsArrayList = new ArrayList<>();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("name");
                String description = object.getString("description");
                String attachment = object.getString("attachment");
                News news = new News(name, description, attachment);
                newsArrayList.add(news);
            }
            NewsAdapter adapter = new NewsAdapter(newsArrayList, getActivity());
            newsList.setAdapter(adapter);
        } catch (Exception ex){
            Toast.makeText(getActivity(), "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            Log.d("response", ex.getMessage());
        }
    }
}
