package jimmy.sanmarkscoutgroup;

public class News {

    String name;
    String description;
    String fileURL;

    public News(String name, String description, String fileURL) {
        this.name = name;
        this.description = description;
        this.fileURL = fileURL;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }
}
