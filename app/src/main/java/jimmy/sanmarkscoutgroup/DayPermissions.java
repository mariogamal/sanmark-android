package jimmy.sanmarkscoutgroup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DayPermissions extends Fragment implements DatePickerDialog.OnDateSetListener {

    TextView permDate;
    ListView permList;

    public DayPermissions() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_day_permissions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        permDate = view.findViewById(R.id.perm_date);
        permDate.setOnClickListener(v -> openPicker());
        permDate.setText(year+"/"+month+"/"+day);
        permList = view.findViewById(R.id.perm_list);
        getData();
    }

    public void openPicker() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        permDate.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_DAY_PERMS,
                response -> {
                    Log.d("Response", response);
                    fetchResult(response);
                },
                error -> Toast.makeText(getActivity(), "تعذر الاتصال", Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("perm_date",permDate.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void fetchResult(String result){
        try
        {
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("boy_name");
                String perm = object.getString("perm");
                names.add(name+"\n"+perm);
            }
            populateList(names);

        }catch (Exception ex)
        {
            Toast.makeText(getActivity(), "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<>());
        }
    }

    public void populateList(ArrayList<String> names)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, names);
        permList.setAdapter(adapter);
    }
}
