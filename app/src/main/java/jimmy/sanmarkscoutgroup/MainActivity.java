package jimmy.sanmarkscoutgroup;

import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView titleText;
    DrawerLayout drawer;
    AppBarLayout appBarLayout;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appBarLayout = findViewById(R.id.app_bar_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        titleText = findViewById(R.id.title_text);
        getSupportActionBar().setTitle("");
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.clerical:
                openFragment(new ClericalFragment());
                break;

            case R.id.news:
                DisplayNews.type = "1";
                openFragment(new DisplayNews());
                break;

            case R.id.files:
                openFragment(new SectionList());
                break;

            case R.id.days:
                DisplayNews.type = "2";
                openFragment(new DisplayNews());
                break;

            case R.id.camps:
                DisplayNews.type = "3";
                openFragment(new DisplayNews());
                break;

            case R.id.parties:
                DisplayNews.type = "4";
                openFragment(new DisplayNews());
                break;

            case R.id.forms:
                DisplayNews.type = "9";
                openFragment(new DisplayNews());
                break;
        }
        menuItem.setChecked(true);
        drawer.closeDrawers();
        titleText.setText(menuItem.getTitle());
        return true;
    }

    private void openFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 && !(getSupportFragmentManager().findFragmentById(R.id.content) instanceof ClericalFragment))
            getSupportFragmentManager().beginTransaction().replace(R.id.content, new ClericalFragment()).commit();
        else
            super.onBackPressed();

    }
}
