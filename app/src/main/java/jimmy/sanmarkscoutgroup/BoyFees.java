package jimmy.sanmarkscoutgroup;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BoyFees extends Fragment {

    RadioGroup sections;
    EditText name;
    ListView boysListView;
    ArrayList<Boy> boysDataList;
    String section;

    public BoyFees() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_boy_fees, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        name = view.findViewById(R.id.name);
        boysListView = view.findViewById(R.id.boys_list);
        sections = view.findViewById(R.id.sections);
        sections.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = group.findViewById(checkedId);
            section = radioButton.getText().toString();
            getData();
        });
        name.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {searchNfilter(s.toString()); }
        });
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.GET_BOYS,
                this::fetchResult,
                error -> Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("condition", "where section = '" + section + "'");
                Log.d("section ", section);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void fetchResult(String response) {
        try {
            boysDataList = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String id = object.getString("id");
                String name = object.getString("name");
                String phone = object.getString("phone");
                String birthdate = object.getString("birthdate");
                String grade = object.getString("grade");
                String address = object.getString("address");
                String section = object.getString("section");
                String fees = object.getString("fees");

                Boy boy = new Boy(id, name, phone, birthdate, grade, address, section, fees);
                boysDataList.add(boy);
                names.add(name+"\n"+fees+" جنية ");
            }
            populateList(names, boysDataList);

        } catch (Exception ex) {
            populateList(new ArrayList<>(), new ArrayList<>());
            Toast.makeText(getActivity(), getResources().getString(R.string.error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void searchNfilter(String key) {
        ArrayList<Boy> boys = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        for (Boy boy : boysDataList) {
            if (boy.getName().contains(key)) {
                boys.add(boy);
                names.add(boy.getName());
            }
        }
        populateList(names, boys);
    }

    public void populateList(ArrayList<String> names, final ArrayList<Boy> boys) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, names);

        boysListView.setAdapter(adapter);
        boysListView.setOnItemClickListener((parent, view, position, id) -> {
            AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.fees_dialog, null);
            Boy boy = boys.get(position);
            EditText feesText = dialogView.findViewById(R.id.fees);
            feesText.setText(boy.getFees());
            dialogView.findViewById(R.id.save).setOnClickListener(v -> {
                addFees(boy.getId(), feesText.getText().toString());
                dialog.dismiss();
            });
            dialog.setView(dialogView);
            dialog.show();
        });
    }

    public void addFees(String id, String fees) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.ADD_FEES,
                response -> {
                    Log.d("Response", response);
                    if (response.contains("true")) {
                        Toast.makeText(getActivity(), "تم اضافة الاشتراك", Toast.LENGTH_SHORT).show();
                        getData();                    }

                },
                error -> Toast.makeText(getActivity(), "تعذر الاتصال", Toast.LENGTH_SHORT).show()
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id", id);
                params.put("fees",fees);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
